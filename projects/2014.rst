.. _gsoc-2014-projects:

:far:`calendar-days` 2014
#########################

BotSpeak PRU Firmware
*********************

.. youtube:: 8g8e4AgDqNo
   :width: 100%

| **Summary:** Developing a remoteproc firmware which will allow high level languages to access PRU (programmable real time unit) functionality on the Beaglebone Black via remote procedure calls. The firmware on the PRU will support a small interpreter which can receive code fragments from the client language and execute them in a loop. The scripting language is similar to Chris rogers Botspeak. Developing this library will allow language independent runtime control over the PRU. 

**Contributor:** Deepak Karki

**Mentors:** Pantelis Antoniou, Hunyue Yau, Jason Kridner, Alex Hiam

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2014/orgs/beagle/projects/deepak_karki.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/deepakkarki/pruspeak
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub repo

PyBBIO Library Development
***************************

.. youtube:: JChsenGXI68
   :width: 100%

| **Summary:** Developing the PyBBIO library to include an SPI Library, Library for interfacing a camera and various sensors such as pressure sensor etc. Adding C extension to Python system calls to make the code run faster. Interfacing a BLE module and an example code to log sensor values on Xively's feed.

**Contributor:** Rekha Seethamraju

**Mentors:** Alexander Hiam, Steve French

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2014/orgs/beagle/projects/rseetham.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/rseetham/PyBBIO
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub repo

BeaglePilot
************

.. youtube:: -giV6Xr8RtY
   :width: 100%

| **Summary:** Create the first Linux-based autopilot for flying robots using the BeagleBone and the BeagleBone Black as the "hardware blueprint". For this purpose I will focus on integrating ArduPilot (most popular autopilot) in the BeagleBone (Black). The main tasks performed will be userspace drivers development, the Robot Operative System Integration (ROS), web IDE exploration and security assessment.

**Contributor:** Victor Mayoral Vilches

**Mentors:** Andrew tridgell,Hunyue Yau, Luis Gustavo Lira

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2014/orgs/beagle/projects/vmayoral.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/BeaglePilot
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub repos

BeagleLogic
************

.. youtube:: CDbEAq33vdA
   :width: 100%

| **Summary:** The proposal aims to create a standalone Logic Analyzer as a debugging and learning tool using the Programmable Real-Time Unit available on the BeagleBone Black.

**Contributor:** Abhishek Kumar

**Mentors:** Matt Ranostay, Hunyue Yau, Charles Steinkehler

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2014/orgs/beagle/projects/abhishek_kakkar.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/abhishek-kakkar/BeagleLogic
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub repo

Bone101
********

.. youtube:: CIugTSnqcf4
   :width: 100%

| **Summary:** I'm going to develop a new platform for creating beagleboard interactive tutorials.This platform will be develop and design in an interactive way allowing users to easily create tutorials. The platform will allow all type of users to create interactive tutorials for sharing with the beagleboard community. The difference between other platform will be that this platform will allow the user to run the code if they have a board. This is a great advantage allowing user to run the tutorials live.

**Contributor:** Diego Turcios Lara

**Mentors:** Jason Krinder, Steve French, David Scheltema and Jongseuk Lee

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2014/orgs/beagle/projects/diegoturcios.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/DiegoTc/bone101
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub repo


Beagle Andriod Remote Display (BARD)
*************************************

.. youtube:: gWzg2dJ0tfA
   :width: 100%

| **Summary:**  “One cable to rule them all”. I intend to build a system where all the basic peripherals - keyboard, mouse and display, can be connected to BeagleBone by attaching it with a USB cable to Android phone. This will be implemented as a kernel module and an ready to use out-of-box app for Android. This module shall have a greater reach and also serve as seed for extending to other systems - Windows, iOS, Ubuntu phones etc.

**Contributor:** Praveen Kumar Pendyala

**Mentors:** Vladimir Pantelic 

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2014/orgs/beagle/projects/praveendath92.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/pkpio/bard-linux
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub repo

.. tip:: 

   Checkout eLinux page for GSoC 2014 projects `here <https://elinux.org/BeagleBoard/GSoC/2014_Projects>`_ for more details.
