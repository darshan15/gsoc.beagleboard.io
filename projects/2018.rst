.. _gsoc-2018-projects:

:far:`calendar-days` 2018
#########################

BeagleBoot
***********

.. youtube:: rlj-k4zyPN0
   :width: 100%

| **Summary:** Integrate node-beagle-boot to Etcher and add features like U-boot console on boot up, TCT/IP proxy server, grab latest images from beagleboard.org to BeagleBoot (electron app), developed last year during GSoC.

**Contributor:** Ravi Kumar Prasad

**Mentors:** Jason Kridner, Abhishek Kumar

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2018/projects/4610302519803904
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/etcher-beagleboot
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal


Fixing Bugs in BoneScript and Improve BeagleBone UI
***************************************************

.. youtube:: ZKOccMBiL5s
   :width: 100%

| **Summary:** BoneScript is an existing Node.js library specifically optimized for the Beagle family and featuring familiar Arduino function calls, exported to the browser.The BoneScript library provides several functions for easy interaction with hardware which otherwise would have required the users to manipulate the SysFs files directly(assuming use of no other libraries),which is obviously confusing for a beginner-level user.

**Contributor:** Vaishnav M.A.

**Mentors:** Jason Kridner, Anuj Deshpande, Micheal Welling

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2018/projects/4871854686732288
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/fix-bugs-bonescript
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Modern Speak and Spell using PocketBeagle
*******************************************

.. youtube:: EiBgch4M_RI
   :width: 100%

| **Summary:** This project is basically to rebrain the previous Speak and Spell by Texas Instruments, and generate open source code for it . This is a Linux application which can be reproduced. The added features include offline speech recognition...using CMU Sphinx...to provide more robust features.

**Contributor:** Anirban Banik

**Mentors:** Eric Weish, Anuj Deshpande, Zubeen Tolani, Hunyue Yau, Andrew Henderson

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2018/projects/5031706255949824
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/Modern_Speak_and_Spell
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal


Update to PyPRUSS - Python API for the PRUs
********************************************

.. youtube:: vHxpa6JwqqI
   :width: 100%

| **Summary:** The PyPRUSS is a python binding/API for controlling the PRUs . It provides an easy to use python-based interface for loading firmware, controlling execution and interrupts/memory management for the PRUs, therefore shortening the learning curve for users new to PRU programming. Currently, the PyPRUSS uses PASM for its examples and communicates using the older Userspace IO (UIO) Driver.

**Contributor:** Mohammed Muneeb

**Mentors:** Kumar Abhishek, Patryk Mężydło, ZeekHuge, S. Lockwood-Childs

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2018/organizations/5713309579870208
         :color: light
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/PyPRUSS
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

.. tip:: 

   Checkout eLinux page for GSoC 2018 projects `here <https://elinux.org/BeagleBoard/GSoC/2018_Projects>`_ for more details.